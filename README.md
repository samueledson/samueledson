# samueledson

- 👋 Olá, eu sou o @samueledson, trabalho com desenvolvimento web desde 2008. Fui de Programador a Analista de Sistemas principalmente desenvolvendo aplicações comerciais com PHP.
- 👀 Eu sou apaixonado por tecnologia, aplicativos, música e viciado em podcasts (de tudo que é assunto).
- 🌱 Atualmente estou estudando Java, Node, React.js, e em breve quero me aventurar no mundo .NET. Minha intenção é ser especialista em Java, mas saber um pouquinho de todo o universo Dev.

- Aqui eu compartilho alguns projetos. Dê uma olhadinha e entre em contato para qualquer dúvida.
